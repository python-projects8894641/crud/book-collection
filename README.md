# book-collection

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/python-projects8894641/crud/book-collection.git
git branch -M main
git push -uf origin main
```

## Introduction

A python project that uses crud operations to create a collection of books with the help of flask and sql as database.

Before running the application, make sure to set the `FLASK_APP` environment variable to `src`:

```bash
export FLASK_APP=src
```

If you're using PowerShell, you can set it with the following command:

```powershell
$env:FLASK_APP = "src"
```
