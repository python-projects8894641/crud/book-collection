from flask import render_template, flash, redirect, url_for, Blueprint, request
from flask_login import login_user, logout_user, current_user, login_required
from src import db
from src.accounts.forms import RegistrationForm, LoginForm
from src.accounts.models import User

accounts_bp = Blueprint('accounts', __name__)


@accounts_bp.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('core.index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        existing_user = User.query.filter((User.username == form.username.data) | (
            User.email == form.email.data)).first()
        if existing_user:
            flash(
                'A user with this username or email already exists. Please register with different credentials.')
            return redirect(url_for('accounts.register'))
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('accounts.login'))
    return render_template('accounts/register.html', title='Register', form=form)


@accounts_bp.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('core.index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(
            username=form.username.data, email=form.email.data).first()
        if user is None:
            flash('Username/Email does not exist. Please register first.', 'error')
            return redirect(url_for('accounts.login'))
        elif not user.check_password(form.password.data):
            flash('Invalid password. Please try again.', 'error')
            return redirect(url_for('accounts.login'))
        login_user(user, remember=form.remember_me.data)
        flash('You have been logged in successfully!', 'success')
        return redirect(url_for('core.index'))
    elif request.method == 'POST':
        flash('Please correct the errors below.', 'error')
    return render_template('accounts/login.html', title='Sign In', form=form)


@accounts_bp.route('/logout', methods=['GET', 'POST'])
def logout():
    logout_user()
    return redirect(url_for('core.dashboard'))


@accounts_bp.route('/profile')
@login_required
def profile():
    return render_template('accounts/profile.html')
