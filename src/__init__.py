from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_migrate import Migrate

db = SQLAlchemy()
login_manager = LoginManager()
migrate = Migrate()


def create_app():
    app = Flask(__name__, static_folder='static', template_folder='templates')
    app.secret_key = 'kakashi'
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+mysqlconnector://root:purplesql@localhost/books'

    db.init_app(app)
    login_manager.init_app(app)
    migrate.init_app(app, db)

    from src.accounts.views import accounts_bp
    from src.core.views import core_bp

    app.register_blueprint(accounts_bp)
    app.register_blueprint(core_bp)

    @login_manager.user_loader
    def load_user(user_id):
        from src.accounts.models import User
        return User.query.get(int(user_id))

    return app
