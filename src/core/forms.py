from flask_wtf import FlaskForm
from wtforms import DateField, StringField, TextAreaField, SubmitField
from wtforms.validators import DataRequired
from flask_wtf.file import FileField, FileAllowed


class BookForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    author = StringField('Author', validators=[DataRequired()])
    image = FileField('Image', validators=[FileAllowed(['jpg', 'png'])])
    description = TextAreaField('Description', validators=[DataRequired()])
    published_date = DateField(
        'Published Date', format='%Y-%m-%d', validators=[DataRequired()])
    submit = SubmitField('Create')
