from datetime import datetime
from src import db


class Book(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    title = db.Column(db.String(100), nullable=False)
    image = db.Column(db.LargeBinary)
    image_filename = db.Column(db.String(100))
    author = db.Column(db.String(100), nullable=False)
    description = db.Column(db.Text, nullable=False)
    published_date = db.Column(
        db.DateTime, nullable=False, default=datetime.utcnow)

    def __repr__(self):
        return f"Book('{self.title}', '{self.author}', '{self.published_date}')"
