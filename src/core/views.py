from io import BytesIO
from flask import render_template, Blueprint, redirect, url_for, flash, abort, request
from flask_login import current_user, login_required
from PIL import Image
import base64

from src import db
from src.core.forms import BookForm
from src.core.models import Book
from src.utils import save_image


core_bp = Blueprint('core', __name__)


@core_bp.route('/')
def home():
    if current_user.is_authenticated:
        return redirect(url_for('core.index'))
    return redirect(url_for('core.dashboard'))


@core_bp.route('/index')
def index():
    books = Book.query.all()
    print(books)
    books_data = []
    for book in books:
        image_data = base64.b64encode(book.image).decode('ascii')
        books_data.append({'book': book, 'image_data': image_data})
    return render_template('core/index.html', books_data=books_data)


@core_bp.route('/dashboard')
def dashboard():
    if current_user.is_authenticated:
        return redirect(url_for('core.index'))
    return render_template('core/dashboard.html')


@core_bp.route('/create', methods=['GET', 'POST'])
@login_required
def create():
    form = BookForm()
    default_img = Image.open('src/assets/default.jpg')
    default_img.thumbnail((300, 300))
    default_image_stream = BytesIO()
    default_img.save(default_image_stream, format='JPEG')
    default_image_stream.seek(0)
    default_image = base64.b64encode(
        default_image_stream.read()).decode('ascii')
    if form.validate_on_submit():
        book = Book(title=form.title.data, author=form.author.data, description=form.description.data,
                    published_date=form.published_date.data, user_id=current_user.id)
        if form.image.data:
            img = Image.open(form.image.data)
            img.thumbnail((300, 300))
            image_stream = BytesIO()
            img.save(image_stream, format='JPEG')
            image_stream.seek(0)
            book.image = image_stream.read()
        else:
            default_image_stream.seek(0)
            book.image = default_image_stream.read()
        db.session.add(book)
        db.session.commit()
        flash('Your book has been created!', 'success')
        return redirect(url_for('core.home'))
    return render_template('core/create_book.html', title='Create Book', form=form, default_image=default_image)


@core_bp.route('/book_detail/<int:book_id>', methods=['GET'])
@login_required
def book_detail(book_id):
    book = Book.query.get_or_404(book_id)
    image_data = base64.b64encode(book.image).decode(
        'ascii') if book.image else None
    return render_template('core/book_detail.html', title='Book Detail', book_data=book, image_data=image_data)


@core_bp.route('/about')
def about():
    return render_template('core/about.html')


@core_bp.route('/delete', methods=['GET', 'POST'])
@login_required
def delete():
    books = Book.query.filter_by(user_id=current_user.id).all()
    books_data = []
    for book in books:
        image_data = base64.b64encode(book.image).decode(
            'ascii') if book.image else None
        books_data.append({'book': book, 'image': image_data})
    if request.method == 'POST':
        book_id = request.form.get('book_id')
        book = Book.query.get(book_id)
        if book:
            db.session.delete(book)
            db.session.commit()
        return redirect(url_for('core.index'))
    return render_template('core/delete_book.html', title='Delete Books', books=books_data)


@core_bp.route('/delete_book/<int:book_id>', methods=['GET', 'POST'])
@login_required
def delete_book(book_id):
    book = Book.query.get_or_404(book_id)
    image_data = base64.b64encode(book.image).decode(
        'ascii') if book.image else None
    return render_template('core/delete_book.html', title='Delete Book', book_data=book, image_data=image_data)


@core_bp.route('/edit', methods=['GET'])
@login_required
def edit():
    books = Book.query.filter_by(user_id=current_user.id).all()
    for book in books:
        book.image = base64.b64encode(book.image).decode(
            'ascii') if book.image else None
    return render_template('core/edit.html', title='Edit Books', books=books)


@core_bp.route('/edit_book/<int:book_id>', methods=['GET', 'POST'])
@login_required
def edit_book(book_id):
    book = Book.query.get_or_404(book_id)
    if book.user_id != current_user.id:
        abort(403)
    form = BookForm()
    if form.validate_on_submit():
        book.title = form.title.data
        book.author = form.author.data
        book.description = form.description.data
        book.published_date = form.published_date.data
        if form.image.data:
            img = Image.open(form.image.data)
            img.thumbnail((300, 300))
            image_stream = BytesIO()
            img.save(image_stream, format='JPEG')
            image_stream.seek(0)
            book.image = image_stream.read()
        db.session.commit()
        flash('Your book has been updated!', 'success')
        return redirect(url_for('core.index'))
    elif request.method == 'GET':
        form.title.data = book.title
        form.author.data = book.author
        form.description.data = book.description
        form.published_date.data = book.published_date
    image_data = base64.b64encode(book.image).decode('ascii')
    return render_template('core/edit_book.html', title='Edit Book', form=form, legend='Edit Book', image_data=image_data)
