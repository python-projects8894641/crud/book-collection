import os
import secrets
from PIL import Image
from flask import current_app


def save_image(image):
    random_hex = secrets.token_hex(8)
    _, file_extension = os.path.splitext(image.filename)
    filename = random_hex + file_extension
    image_dir = os.path.join(current_app.root_path, 'static/book_images')

    if not os.path.exists(image_dir):
        os.makedirs(image_dir)

    image_path = os.path.join(image_dir, filename)

    output_size = (125, 125)
    i = Image.open(image)
    i.thumbnail(output_size)
    i.save(image_path)

    return filename
