document
  .querySelector('input[type="file"]')
  .addEventListener("change", function (event) {
    var reader = new FileReader();
    reader.onload = function (event) {
      document.querySelector("#preview").src = event.target.result;
    };
    reader.readAsDataURL(event.target.files[0]);
  });
